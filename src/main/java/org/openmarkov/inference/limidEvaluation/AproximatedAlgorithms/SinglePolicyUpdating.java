/*
 * Copyright 2013 CISIAD, UNED, Spain
 *
 * Licensed under the European Union Public Licence, version 1.1 (EUPL)
 *
 * Unless required by applicable law, this code is distributed
 * on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.limidEvaluation.AproximatedAlgorithms;

//import java.util.ArrayList;
//import java.util.Collection;
import org.openmarkov.core.exception.*;
import org.openmarkov.core.inference.InferenceAlgorithm;
import org.openmarkov.core.inference.PartialOrder;
import org.openmarkov.core.inference.annotation.InferenceAnnotation;
import org.openmarkov.core.inference.heuristic.EliminationHeuristic;
import org.openmarkov.core.model.network.*;
import org.openmarkov.core.model.network.constraint.PNConstraint;
import org.openmarkov.core.model.network.potential.*;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
import org.openmarkov.core.model.network.type.LIMIDType;
import org.openmarkov.core.model.network.type.NetworkType;
import org.openmarkov.inference.heuristic.minimalFillIn.MinimalFillIn;
import org.openmarkov.inference.heuristic.simpleElimination.SimpleElimination;
import org.openmarkov.inference.huginPropagation.HuginForest;
import org.openmarkov.inference.variableElimination.action.CRemoveNodeVEEdit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 *
 * Single Policy Updating algorithm for Limited Memory Influence Diagrams.
 * 
 * Pseudocode obtained from nilsson2005
 * (The updating of the policy for decision d, where d is assigned 
 * to root-clique R.)
 * 
 * 
 * Step 1: Retract the policy for d from the potential on R to obtain ˜φR.
 * 
 * Step 2: Collect messages to R to obtain φ∗R.
 * 
 * Step 3: Compute φ∗ fa(d) = (φ∗R)↓fa(d).
 * 
 * Step 4: Compute the contraction cfa(d) of φ∗ fa(d).
 * 
 * Step 5: For each xpa(d), find a configuration x∗d ∈ Xd satisfying
 *              x∗d = argmax xd cfa(d)(xd, xpa(d)),
 * and define ˜δd(xd, xpa(d)) as 1 if xd = x∗d and 0 otherwise.
 * 
 * 6. Step 6: Add ˜δd to the potential on R.
 * 
 * @author martaso
 * @version 1.0
 * 
 */

@InferenceAnnotation(name = "SinglePolicyUpdating")
public class SinglePolicyUpdating extends InferenceAlgorithm {

    /**
     * @param probNet The network used in the inference
     * @throws org.openmarkov.core.exception.NotEvaluableNetworkException
     */
    public SinglePolicyUpdating(ProbNet probNet) throws NotEvaluableNetworkException {
        super(probNet);
    }

    /**
     * @return The optimal strategy
     * @throws org.openmarkov.core.exception.UnexpectedInferenceException
     * @throws org.openmarkov.core.exception.IncompatibleEvidenceException
     */
    @Override
    public Intervention getOptimalStrategy() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @param decisionVariable
     * @return The optimal policy for the decision that does not have any imposed policy.
     * The domain of the policy also includes the decision and the conditioning variables.
     */
    @Override
    public Potential getOptimizedPolicy(Variable decisionVariable) throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @param decisionVariable
     * @return The expected utilities of the optimal policy for the decision that does not have any imposed policy.
     * The domain of the policy also includes the decision and the conditioning variables.
     */
    @Override
    public Potential getExpectedUtilities(Variable decisionVariable) throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @return The global expected utility of the influence diagram. It is a potential
     * defined over the conditioning variables.
     */
    @Override
    public TablePotential getGlobalUtility() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @return The posterior probabilities and utilities of the network.
     * @throws org.openmarkov.core.exception.IncompatibleEvidenceException
     * @throws org.openmarkov.core.exception.NormalizeNullVectorException
     */
    @Override
    public HashMap<Variable, TablePotential> getProbsAndUtilities() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @param variablesOfInterest
     * @return The posterior probabilities and utilities of the network.
     * @throws org.openmarkov.core.exception.IncompatibleEvidenceException
     * @throws org.openmarkov.core.exception.NormalizeNullVectorException
     */
    @Override
    public HashMap<Variable, TablePotential> getProbsAndUtilities(List<Variable> variablesOfInterest) throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @param variables
     * @return The joint probability of a list of variables
     * @throws org.openmarkov.core.exception.IncompatibleEvidenceException
     * @throws org.openmarkov.core.exception.NormalizeNullVectorException
     */
    @Override
    public TablePotential getJointProbability(List<Variable> variables) throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }
}