/*
 * Copyright 2013 CISIAD, UNED, Spain
 *
 * Licensed under the European Union Public Licence, version 1.1 (EUPL)
 *
 * Unless required by applicable law, this code is distributed
 * on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.limidEvaluation.AproximatedAlgorithms;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.InferenceAlgorithm;
import org.openmarkov.core.inference.annotation.InferenceAnnotation;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.Intervention;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;

import java.util.HashMap;
import java.util.List;

/**
 *
 * Single Rule Updating algorithm for Limited Memory Influence Diagrams.
 *
 * @author martaso
 * @version 1.0
 * 
 */

@InferenceAnnotation(name = "SingleRuleUpdating")
public class SingleRuleUpdating extends InferenceAlgorithm {

    /**
     * @param probNet The network used in the inference
     * @throws org.openmarkov.core.exception.NotEvaluableNetworkException
     */
    public SingleRuleUpdating(ProbNet probNet) throws NotEvaluableNetworkException {
        super(probNet);
    }

    /**
     * @return The optimal strategy
     * @throws org.openmarkov.core.exception.UnexpectedInferenceException
     * @throws org.openmarkov.core.exception.IncompatibleEvidenceException
     */
    @Override
    public Intervention getOptimalStrategy() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @param decisionVariable
     * @return The optimal policy for the decision that does not have any imposed policy.
     * The domain of the policy also includes the decision and the conditioning variables.
     */
    @Override
    public Potential getOptimizedPolicy(Variable decisionVariable) throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @param decisionVariable
     * @return The expected utilities of the optimal policy for the decision that does not have any imposed policy.
     * The domain of the policy also includes the decision and the conditioning variables.
     */
    @Override
    public Potential getExpectedUtilities(Variable decisionVariable) throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @return The global expected utility of the influence diagram. It is a potential
     * defined over the conditioning variables.
     */
    @Override
    public TablePotential getGlobalUtility() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @return The posterior probabilities and utilities of the network.
     * @throws org.openmarkov.core.exception.IncompatibleEvidenceException
     * @throws org.openmarkov.core.exception.NormalizeNullVectorException
     */
    @Override
    public HashMap<Variable, TablePotential> getProbsAndUtilities() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @param variablesOfInterest
     * @return The posterior probabilities and utilities of the network.
     * @throws org.openmarkov.core.exception.IncompatibleEvidenceException
     * @throws org.openmarkov.core.exception.NormalizeNullVectorException
     */
    @Override
    public HashMap<Variable, TablePotential> getProbsAndUtilities(List<Variable> variablesOfInterest) throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    /**
     * @param variables
     * @return The joint probability of a list of variables
     * @throws org.openmarkov.core.exception.IncompatibleEvidenceException
     * @throws org.openmarkov.core.exception.NormalizeNullVectorException
     */
    @Override
    public TablePotential getJointProbability(List<Variable> variables) throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }
}