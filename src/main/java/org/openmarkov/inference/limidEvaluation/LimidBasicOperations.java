/*
 * Copyright 2013 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.limidEvaluation;

import org.openmarkov.core.exception.ConstraintViolationException;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.exception.WrongGraphStructureException;
import org.openmarkov.core.model.graph.Link;
import org.openmarkov.core.model.network.*;
import org.openmarkov.core.model.network.constraint.OnlyDirectedLinks;
import org.openmarkov.core.model.network.constraint.OnlyUndirectedLinks;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.type.MarkovNetworkType;

import java.util.*;

/**
 *
 * Basic operations for Limited Memory Influence Diagrams.
 *
 * @author martaso
 * @version 1.0
 *
 */

public final class LimidBasicOperations {

    private enum Direction {
        UP, DOWN
    }


    private static class NodeDirection {
        private Node node;
        private Direction direction;

        public NodeDirection (Node node, Direction direction) {
            this.node = node;
            this.direction = direction;
        }

        protected Node getNode() {
            return node;
        }

        protected Direction getDirection() {
            return direction;
        }

    }


    /**
     *
     * Adapted from [koller2009, algorithm 3.1]
     *
     * A trail (t) (a sequence of nodes that are connected
     * by arcs, not necessarily following the directions) from
     * node (a) to node (b) in a DAG (D) is said to be blocked
     * by S if it contains a node (n) ∈ (t) such that either n ∈ S
     * and arcs of (t) do not meet head-to-head at n, or n and
     * all its descendants are not in S, and arcs of (t) meet
     * head-to-head at n. A trail that is not blocked by S is
     * said to be active. Two subsets A and B of nodes are
     * d-separated by S if all trails from A to B are blocked
     * by S.
     *
     * A path between two nodes is always active
     *
     * There are four kinds of paths when three nodes are involved.
     *
     * Three of them are:
     *
     * A -> N -> C
     * A <- N <- C
     * A <- N -> C
     *
     * In this kind of paths, if N belongs to S, the path is
     * blocked by S. Or,in other words, the path is active iif N is not active
     *
     * The last option is:
     *
     * A -> N <- C
     *
     * In this case, if N and its descendants are not in S, the
     * path is blocked by S.
     *
     * @param aSet One of the vertex of the path
     * @param bSet The other vertex of the path
     * @param cSet The set of observed variables
     *
     */

    // Básicamente, en los tres primeros casos,
    // Si en el camino que pasa por X los enlaces no se encuentran
    // cabeza con cabeza y X no pertenece a C, el camino está activo

    // Si en el camino que pasa por X los enlaces se encuentran
    // cabeza con cabeza y si X o alguno de sus descendientes
    // pertenece a C, el camino está activo.

    // Pag. 74 Koller, algoritmo.

    // Es más fácil si se entiende a C como las vbles observadas...

    public static boolean dSeparared(List<Node> aSet, List<Node> bSet, List<Node> cSet) {

        boolean dSeparated = true;

        Iterator<Node> aIterator = aSet.iterator();

        while (dSeparated && aIterator.hasNext()) {
            Iterator<Node> bIterator = bSet.iterator();

            // Base case, if A and B are directly connected, the path is open and they are not d-separated

            Iterator<Node> rIterator = reachableNodes(aIterator.next(), cSet).iterator();
            while (dSeparated && bIterator.hasNext()) {
                Node b = bIterator.next();
                while (dSeparated && rIterator.hasNext()) {
                    if (b.equals(rIterator.next())) {
                        dSeparated = false;
                    }
                }
            }
        }

        return dSeparated;

    }

    // TODO: to put this in the core
    /**
     * Adapted from [koller2009, algorithm 3.1]
     * @param a Source node
     * @param cSet Set of observations
     * @return A list of reachable variables
     */
    public static List<Node> reachableNodes(Node a, List<Node> cSet) {

        List<Node> reachableNodes = new ArrayList<>(); //List of reachable nodes
        Stack<Node> nodesToVisit = new Stack<>(); //List of nodes to be visited
        Node nodeToVisit;

        Stack<NodeDirection> nodesDirection = new Stack<>();

        List<NodeDirection> visitedNodesDirection = new ArrayList<>(); //List of visited nodes

        NodeDirection nodeDirection;

        List<Node> ancestorsC = new ArrayList<>(); //List of ancestors of the nodes of cSet

        nodesToVisit.addAll(cSet);

        // Phase I from [koller2009]: Insert all ancestors of Z into V
        while(!nodesToVisit.isEmpty()) {
            // Select some node from the list
            nodeToVisit = nodesToVisit.pop();
            if (!ancestorsC.contains(nodeToVisit)) {
                // The node's parents need to be visited
                for (Node nodeToVisitParent : nodeToVisit.getParents()) {
                    if (!nodesToVisit.contains(nodeToVisitParent)) {
                        nodesToVisit.add(nodeToVisitParent);
                    }
                }
            }
            // The node is ancestor of evidence
            if (!ancestorsC.contains(nodeToVisit)) {
                ancestorsC.add(nodeToVisit);
            }
        }

        //Phase II from [koller2009]: traverse active trails starting from X
        nodesDirection.clear();
        // The pair (Node,direction) to be visited
        nodesDirection.push(new NodeDirection(a, Direction.UP));
        // The list for pairs (Node,direction) visited
        visitedNodesDirection.clear();
        // List for the nodes reachable via active trail
        reachableNodes.clear();

        while (!nodesDirection.isEmpty()) {
            // A pair (Node,direction) is selected from the list
            nodeDirection = nodesDirection.pop();
            if (!visitedNodesDirectionContainsNode(visitedNodesDirection,nodeDirection)) { //!visitedNodesDirection.contains(nodeDirection)
                if (!cSet.contains(nodeDirection.getNode())) {
                    if (!reachableNodes.contains(nodeDirection.getNode())) {
                        reachableNodes.add(nodeDirection.getNode()); // The node is reachable
                    }
                }
                visitedNodesDirection.add(nodeDirection); //Mark the pair (Node,direction) as visited
                if (nodeDirection.getDirection().equals(Direction.UP) &&
                        !cSet.contains(nodeDirection.getNode())) { // Trail up through Node active if Node not in cSet (observations)
                    // for each node that belongs to the pair's node fathers
                    for (Node nodeDirectionParent : nodeDirection.getNode().getParents()) {
                        // Pair node's parents to be visited form bottom
                        nodesDirection.add(new NodeDirection(nodeDirectionParent, Direction.UP));
                    }
                    // for each node that belongs to pair's node children
                    for (Node nodeDirectionChild : nodeDirection.getNode().getChildren()) {
                        // Pair's node children to be visited from top
                        nodesDirection.add(new NodeDirection(nodeDirectionChild, Direction.DOWN));
                    }
                } else if (nodeDirection.getDirection().equals(Direction.DOWN)) { //Trails down through pair's node
                    if (!cSet.contains(nodeDirection.getNode())) {
                        // Downward trails to pair's node children are active
                        // for each node that belongs to pair's node children
                        for (Node nodeDirectionChild : nodeDirection.getNode().getChildren()) {
                            // pair's node children to be visited form top
                            nodesDirection.add(new NodeDirection(nodeDirectionChild, Direction.DOWN));
                        }
                    }
                    if (ancestorsC.contains(nodeDirection.getNode())) { // v-structure trails are active
                        // pair's node parents to be visited form bottom
                        // for each node that belongs to pair's node fathers
                        for (Node nodeDirectionParent : nodeDirection.getNode().getParents()) {
                            // pair's node parents to be visited form bottom
                            nodesDirection.add(new NodeDirection(nodeDirectionParent, Direction.UP));
                        }
                    }
                }
            }
        }
        // Reachable nodes are returned
        return reachableNodes;
    }

    private static boolean visitedNodesDirectionContainsNode(List<NodeDirection> visitedNodesDirection, NodeDirection nodeDirection) {
        boolean alreadyVisited = false;
        NodeDirection visitedNodeDirection;
        String nodeDirectionName = nodeDirection.getNode().getName();
        Direction nodeDirectionDirection = nodeDirection.getDirection();
        for (int i = 0 ; i < visitedNodesDirection.size() && !alreadyVisited ; i++) {
            visitedNodeDirection = visitedNodesDirection.get(i);
            if (visitedNodeDirection.getNode().getName().compareTo(nodeDirectionName) == 0 &&
                    visitedNodeDirection.getDirection().equals(nodeDirectionDirection)) {
                alreadyVisited = true;
            }
        }
        return alreadyVisited;
    }

    protected static void getLIMIDversionOfID(ProbNet probNet) {
        ProbNetOperations.addNoForgettingArcs(probNet);
    }

    /**
     * @param probNet a LIMID
     * @return true if the LIMID is soluble in the sense of nilsson2000, false otherwise
     */
    public static boolean isLIMIDsoluble(ProbNet probNet) {

        // The decision have to be extremal
        // An exact solution order can be found

        boolean isSoluble = true;
        boolean extremalNodeFound = false;
        Node extremalNode = null;
        ProbNet solubleProbNet = probNet.copy();

        // Decision nodes of the probabilistic network
        List<Node> decisionNodes = solubleProbNet.getNodes(NodeType.DECISION);
        Iterator decisionNodesIterator = decisionNodes.iterator();

        /* 1. D = decisons;
           2. Search for an extremal decision node d ∈ D; if
              none exists, return null; else
              (a) let D = D\d; Convert D in a CHANCE node;
              (b) if D=, return “min is soluble” and exit
            3. goto 2.
        */

        if (decisionNodes != null) {
            if (decisionNodes.size() > 0) {
                while (decisionNodes.size() > 0 && isSoluble) {
                    // All the decision nodes are iterated
                    for (Node decisionNode : decisionNodes) {
                        // We check if the node is extremal
                        extremalNode = findIfExtremalNode(solubleProbNet, decisionNodes, decisionNode); // (Node) decisionNodesIterator.next());
                        // If it is, we update the boolean variable
                        extremalNodeFound = (extremalNode != null);
                        // And we stop the loop
                        // (it is not the best way to break the loop, but an iterator and a while loop would not work here
                        if (extremalNodeFound) {
                            break;
                        }
                    }
                    // The LIMID is still soluble if an extremal node has been found
                    isSoluble = extremalNodeFound;
                    // If it is still soluble, also, the decision node that is extremal
                    // must be converted to a CHANCE node
                    if (isSoluble) {
                        try {
                            decisionNodes.remove(extremalNode);
                            //decisionNodes.remove(solubleProbNet.getNode(extremalNode.getName()));
                            solubleProbNet.getNode(extremalNode.getName()).setNodeType(NodeType.CHANCE);
                        } catch (NodeNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return isSoluble;
    }

    private static List<Node> getFamiliesOtherDecisionNodes (ProbNet probNet, Node decisionNode, List<Node> decisionNodes) {
        // Variable to be returned
        List <Node> familiesOtherDecisionNodes = new ArrayList<>();
        // Decision nodes of the probabilistic network
        // List<Node> decisionNodes = probNet.getNodes(NodeType.DECISION);
        List<Node> decisionNodesInNet = new ArrayList<>(decisionNodes);
        // The decision node that is being treated is removed from the list
        decisionNodesInNet.remove(decisionNode);
        // We iterate over the remaining decisions of the probabilistic network
        for (Node decision : decisionNodesInNet) { //decisionNodes) {
            // The decision itself is part of the family (if they were not already in the list)
            if (!familiesOtherDecisionNodes.contains(decision)) {
                familiesOtherDecisionNodes.add(decision);
            }
            // Along with its parents (if they were not already in the list)
            for (Node decisionParent : decision.getParents()) {
                if (!familiesOtherDecisionNodes.contains(decisionParent)) {
                    familiesOtherDecisionNodes.add(decisionParent);
                }
            }
        }
        // And return the families of the other decision nodes apart from that being treated
        return familiesOtherDecisionNodes;
    }


    /**
     * A decision node d is extremal in the LIMID L if
     * u ⊥ (⋃ {fa(d) : d ∈ ∆ \ {d0}} | fa(d0)
     * for every utility node u ∈ de(d0)

     * Meaning, a decision node d0 is extremal if every
     * utility node descendant of this decision node is d-separated
     * from the union the families of the decision nodes, less the family of d0 itself, given the family of the decision node

     * Un nodo de decisión *d* es extremal, en el sentido de Lauritzen y Nilsson,
     * si todos los nodos de utilidad que son descendientes de dicho nodo *d* están "d-separados" en
     * el LIMID de la unión de las familias de los nodos de decisión menos la propia familia del nodo *d*
     * y dada la familia de *d*.
     *
     * @param probNet
     * @return any extremal decision in the ProbNet or null if none extremal decision was found
     */
    private static Node findIfExtremalNode(ProbNet probNet, List<Node> decisionNodes, Node decisionNode) {
        Node extremalNode = null;
        boolean extremalNodeFound = true;
        boolean areDSeparated = true;

        // Decision nodes of the probabilistic network
        // List<Node> decisionNodes = probNet.getNodes(NodeType.DECISION);
        List<Node> decisionNodesInNet = new ArrayList<>(decisionNodes);
        Iterator decisionNodesIterator = decisionNodesInNet.iterator(); //decisionNodes.iterator();

        // Node decisionNode = null;

        List<Node> descendantUtilityNodes;
        Iterator descendantUtilityNodesIterator;

        List<Node> aSet;
        List<Node> bSet;
        List<Node> cSet;

        //while (decisionNodesIterator.hasNext() && extremalNodeFound) {
        //decisionNode = (Node)decisionNodesIterator.next();
        descendantUtilityNodes = getUtilityNodesDescendant(probNet,decisionNode);
        descendantUtilityNodesIterator = descendantUtilityNodes.iterator();
        while (descendantUtilityNodesIterator.hasNext() && areDSeparated) {
            // aSet: u
            aSet = Arrays.asList((Node)descendantUtilityNodesIterator.next());
            // bSet: ⋃ {fa(d) : d ∈ ∆ \ {d0}}
            bSet = getFamiliesOtherDecisionNodes(probNet,decisionNode, decisionNodes);
            // cSet: fa(d0)
            cSet = new ArrayList<>(probNet.getParents(decisionNode));
            // The node is extremal if aSet and bSet are d-separated given cSet
            areDSeparated = dSeparared(aSet,bSet,cSet);
        }
        extremalNodeFound = areDSeparated;
        //}
        if (extremalNodeFound) {
            return decisionNode;
        }
        else {
            return null;
        }
    }

    /**
     * [nilsson2000, page 439]
     * @param probNet a soluble LIMID
     * @return A minimal version of the soluble LIMID where all the nodes that are non requisites
     * have been removed
     */
    public static ProbNet reduceSolubleLIMID(ProbNet probNet) {
        ProbNet reducedLIMID = probNet.copy();

        boolean nonRequisite;

        List<Variable> decisionVariables = reducedLIMID.getVariables(NodeType.DECISION);
        List<Variable> orderedDecisionVariables;
        Node orderedDecisionNode;

        List<Node> decisionParents;
        List<Node> familyDecisionNode;
        List<Node> descendantUtilityNodes;
        Iterator descendantUtilityNodesIterator;

        List<Node> aSet;
        List<Node> bSet;
        List<Node> cSet;

        if (decisionVariables.size() > 1) {
            orderedDecisionVariables = ProbNetOperations.sortTopologically(reducedLIMID, decisionVariables);
        }
        else {
            orderedDecisionVariables = decisionVariables;
        }

        // For every decision node i, moving from decision node k to 1, meaning in reverse order,
        // there must be removed the arcs from non-requisite parents of decision node i.
        for (int i = orderedDecisionVariables.size() -1 ; i >= 0 ; i--) {

            orderedDecisionNode = reducedLIMID.getNode(orderedDecisionVariables.get(i));

            // The non-requisite parents are those that do satisfy the following condition:
            // A node n that belong to the group pa(d)
            // u ⊥ n | (fa(d) \ {n})
            // for every utility node that u that belong to the group de(d)

            // In other words, a node n that is a parent of a decision node d that is d-separated
            // of every utility node that is a descendant of that decision node d given the family of d without the
            // node n, is said to be a non-requisite


            decisionParents = reducedLIMID.getParents(orderedDecisionNode);
            familyDecisionNode = new ArrayList<>(decisionParents);
            familyDecisionNode.add(orderedDecisionNode);

            descendantUtilityNodes = getUtilityNodesDescendant(reducedLIMID,orderedDecisionNode);

            // We iterate over the parents of the decision node
            for (Node decisionParent : decisionParents) {
                nonRequisite = true;
                descendantUtilityNodesIterator = descendantUtilityNodes.iterator();
                // The parent has to be d-separated from every utility descendant of the
                // decision to be a non-requisite
                while (descendantUtilityNodesIterator.hasNext() && nonRequisite) {
                    // aSet: u
                    aSet = Arrays.asList((Node)descendantUtilityNodesIterator.next());
                    // bSet: n
                    bSet = Arrays.asList(decisionParent);
                    // cSet: (fa(d) \ {n})
                    cSet = new ArrayList<>(familyDecisionNode);
                    cSet.remove(decisionParent);
                    // The node will be a non-requisite if is d-separated from the utility, given the cSet
                    nonRequisite = dSeparared(aSet,bSet,cSet);
                }
                // If the parent is a non-requisite, its link to the decision can be removed
                if (nonRequisite) {
                    reducedLIMID.removeLink(decisionParent,orderedDecisionNode,true);
                }
            }
        }

        return reducedLIMID;
    }

    private static List<Node> getUtilityNodesDescendant(ProbNet probNet, Node node) {

        Stack<Node> descendantNodes = new Stack<> ();
        List<Node> descendantUtilityNodes = new ArrayList<>();

        descendantNodes.push (node);
        while(!descendantNodes.isEmpty ()) {
            Node descendantNode = descendantNodes.pop();
            // Should the descendant be an utility node, it is added to the list
            if(descendantNode.getNodeType() == NodeType.UTILITY) {
                descendantUtilityNodes.add(descendantNode);
            }
            // The children of the descendant are added to the stack
            for(Node descendant : probNet.getChildren (descendantNode)) {
                descendantNodes.push(descendant);
            }
        }
        return descendantUtilityNodes;
    }
}
