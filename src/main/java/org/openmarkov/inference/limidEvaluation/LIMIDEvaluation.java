/*
 * Copyright 2014 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.limidEvaluation;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.InferenceAlgorithm;
import org.openmarkov.core.inference.annotation.InferenceAnnotation;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.Intervention;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.type.InfluenceDiagramType;
import org.openmarkov.inference.huginPropagation.JunctionTreeForestPropagation;

import java.util.HashMap;
import java.util.List;

/**
 *
 * nilsson2000
 *
 * @author artasom
 */
@InferenceAnnotation(name = "LIMIDversionID")
public class LIMIDEvaluation extends InferenceAlgorithm {

    boolean solubleLIMID = false;

    boolean algorithmRefinement;

    /**
     * @param probNet The network used in the inference
     * @throws org.openmarkov.core.exception.NotEvaluableNetworkException
     */
    public LIMIDEvaluation(ProbNet probNet, boolean algorithmRefinement) throws NotEvaluableNetworkException {
        super(probNet);
        this.algorithmRefinement = algorithmRefinement;
        // If the probabilistic network that has been received is an Influence Diagram
        if (probNet.getNetworkType() == InfluenceDiagramType.getUniqueInstance()) {
            // Then we get the LIMID version of the Influence Diagram
            LimidBasicOperations.getLIMIDversionOfID(this.probNet);
            // If we received an Influence Diagram, its LIMID version is soluble [nilsson200]
            solubleLIMID = true;
        }
        // If we are not still sure if the LIMID is soluble (if it is not the LIMID version of an ID)
        // we have to check whether it is soluble or not.
        if (!solubleLIMID) {
            solubleLIMID = LimidBasicOperations.isLIMIDsoluble(this.probNet);
        }
        // If the LIMID is soluble, then we need to check is a minimal version of it can be obtained
        if (solubleLIMID) {
            // This cannot be done here because otherwise the information about the order of the decisions
            // would not be correct. Now, it is done in ClusterPropagation.java, in the constructor.
            // Many methods needed to be added to this class, that should be moved to somewhere more "central",
            // maybe the core
            // TODO: move this methods to the core.
            // this.probNet = LimidBasicOperations.reduceSolubleLIMID(this.probNet);
            // The moral probNet will be calculated in the inference
            // this.probNet = LimidBasicOperations.getMoralProbNet(this.probNet);
        }
    }

    @Override
    public Intervention getOptimalStrategy() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    @Override
    public Potential getOptimizedPolicy(Variable decisionVariable) throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    @Override
    public Potential getExpectedUtilities(Variable decisionVariable) throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }

    @Override
    public TablePotential getGlobalUtility() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        JunctionTreeForestPropagation algorithmTest = null;
        EvidenceCase evidenceCase1 = new EvidenceCase();
        try {
            algorithmTest = new JunctionTreeForestPropagation(this.probNet,"LIMID"); //network
        } catch (NotEvaluableNetworkException e) {
            e.printStackTrace();
        }
        algorithmTest.setPreResolutionEvidence(evidenceCase1);
        algorithmTest.setLIMIDAlgorithmRefinement(this.algorithmRefinement);
        return algorithmTest.getGlobalUtility();
    }

    @Override
    public HashMap<Variable, TablePotential> getProbsAndUtilities() throws IncompatibleEvidenceException, UnexpectedInferenceException {
        getProbsAndUtilities(null);
        return null;
    }

    @Override
    public HashMap<Variable, TablePotential> getProbsAndUtilities(List<Variable> variablesOfInterest) throws IncompatibleEvidenceException, UnexpectedInferenceException {
        // If the LIMID is soluble, then the inference can be done as in nilsson2000
        if (solubleLIMID) {
            JunctionTreeForestPropagation algorithmTest;
            EvidenceCase evidenceCase1 = new EvidenceCase();
            try {
                algorithmTest = new JunctionTreeForestPropagation(this.probNet,"LIMID"); //"InfluenceDiagram"); //network
                algorithmTest.setLIMIDAlgorithmRefinement(this.algorithmRefinement);
                algorithmTest.setPreResolutionEvidence(evidenceCase1);
                algorithmTest.getProbsAndUtilities();
                double jTreeForest;
                jTreeForest = algorithmTest.getGlobalUtility().values[0];
                //System.out.println();
                //System.out.println("LIMID inference: " + jTreeForest);
            }
            catch (IncompatibleEvidenceException | UnexpectedInferenceException | NotEvaluableNetworkException e) {
                e.printStackTrace();
            }
        }
        // TODO: Should it not be soluble, an approximated solution must be found.
        else {

        }
        return null;
    }

    @Override
    public TablePotential getJointProbability(List<Variable> variables) throws IncompatibleEvidenceException, UnexpectedInferenceException {
        return null;
    }
}
