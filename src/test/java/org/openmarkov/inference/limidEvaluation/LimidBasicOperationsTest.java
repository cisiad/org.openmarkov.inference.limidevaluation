/*
 * Copyright 2013 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.limidEvaluation;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.exception.NonProjectablePotentialException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.WrongCriterionException;
import org.openmarkov.core.inference.InferenceAlgorithm;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.io.probmodel.PGMXReader;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeTrue;

/** @author artasom */

public class LimidBasicOperationsTest { //extends InferenceAlgorithmTests {

    protected double maxError = 1E-10;

    private final Double noExpectedMeu = -1.00;

    private HashMap<String,Double> influenceDiagramsExpectedUtilities = new HashMap<>();
    private HashMap<String,Double> influenceDiagramsExpectedUtilitiesTest = new HashMap<>();

    private static final boolean checkTestID = false;
    //private static final boolean checkTestID = true;

    private static final boolean algorithmRefinement = true;
    //private static final boolean algorithmRefinement = false;

    // Initialization
    @Before
    public void setUp() throws Exception {
        // POMDP probabilistic networks expected utilities
        influenceDiagramsExpectedUtilities.put("BPOMDP/ACIDs-BPOMDP-1.pgmx",1.2577423025406183);
        influenceDiagramsExpectedUtilities.put("BPOMDP/ACIDs-BPOMDP-2.pgmx",1.4084729786407297);
        influenceDiagramsExpectedUtilities.put("BPOMDP/ACIDs-BPOMDP-3.pgmx",2.27407382511335);
        influenceDiagramsExpectedUtilities.put("BPOMDP/ACIDs-BPOMDP-4.pgmx",1.924079614009766);
        influenceDiagramsExpectedUtilities.put("BPOMDP/ACIDs-BPOMDP-5.pgmx",noExpectedMeu);
        influenceDiagramsExpectedUtilities.put("BPOMDP/ACIDs-BPOMDP-6.pgmx",noExpectedMeu);
        influenceDiagramsExpectedUtilities.put("BPOMDP/ACIDs-BPOMDP-7.pgmx",noExpectedMeu);
        influenceDiagramsExpectedUtilities.put("BPOMDP/ACIDs-BPOMDP-8.pgmx",noExpectedMeu);
        influenceDiagramsExpectedUtilities.put("BPOMDP/ACIDs-BPOMDP-9.pgmx",noExpectedMeu);
        influenceDiagramsExpectedUtilities.put("BPOMDP/ACIDs-BPOMDP-10.pgmx",noExpectedMeu);
        // Decision problem probabilistic networks expected utilities
        influenceDiagramsExpectedUtilities.put("DP/ACIDs-DP-1.pgmx",0.7678498705000001);
        influenceDiagramsExpectedUtilities.put("DP/ACIDs-DP-2.pgmx",0.4662215607046728);
        influenceDiagramsExpectedUtilities.put("DP/ACIDs-DP-3.pgmx",0.6661076048299583);
        influenceDiagramsExpectedUtilities.put("DP/ACIDs-DP-4.pgmx",0.45048860400000007);
        influenceDiagramsExpectedUtilities.put("DP/ACIDs-DP-5.pgmx",0.18089334720000005);
        influenceDiagramsExpectedUtilities.put("DP/ACIDs-DP-6.pgmx",0.8962717960000004);
        influenceDiagramsExpectedUtilities.put("DP/ACIDs-DP-7.pgmx",0.7209109666026049);
        influenceDiagramsExpectedUtilities.put("DP/ACIDs-DP-8.pgmx",noExpectedMeu);
        influenceDiagramsExpectedUtilities.put("DP/ACIDs-DP-9.pgmx",noExpectedMeu);
        influenceDiagramsExpectedUtilities.put("DP/ACIDs-DP-10.pgmx",noExpectedMeu);
        // Other influence diagrams
        influenceDiagramsExpectedUtilities.put("other-IDs/id_jensen1994b_withinforlinks.pgmx",98.40973061679536);
        influenceDiagramsExpectedUtilities.put("other-IDs/decide-test.pgmx",9.563199999999998);
        influenceDiagramsExpectedUtilities.put("other-IDs/decide-test-no-2.u.pgmx",96.006);
        influenceDiagramsExpectedUtilities.put("other-IDs/decide-test-no-3.u.pgmx",96.006);
        influenceDiagramsExpectedUtilities.put("other-IDs/diagnosis-problem.pgmx",96.006);
        influenceDiagramsExpectedUtilities.put("other-IDs/drilling.pgmx",22500.00);
        influenceDiagramsExpectedUtilities.put("other-IDs/drilling_2utilities.pgmx",22.5);
        // Test area
        influenceDiagramsExpectedUtilitiesTest.put("other-IDs/drilling.pgmx",22500.00);
        /*influenceDiagramsExpectedUtilitiesTest.put("networkPath/networkName.pgmx",expectedUtilitie);*/
    }

    public InferenceAlgorithm buildInferenceAlgorithm(ProbNet probNet) throws NotEvaluableNetworkException {
        return new LIMIDEvaluation(probNet,algorithmRefinement);
    }

    @Test
    public  void  testInfluenceDiagrams() {

        LIMIDEvaluation inferenceAlgorithmLIMID;
        Double expectedInferenceMeu;
        Double inferenceMeu;

        HashMap<String,Double> influenceDiagrams;

        if (checkTestID) {
            influenceDiagrams = influenceDiagramsExpectedUtilitiesTest;
        }
        else {
            influenceDiagrams = influenceDiagramsExpectedUtilities;
        }

        for (Map.Entry<String, Double> influenceDiagram : influenceDiagrams.entrySet()) {
            try {

                String bayesNetworkName = influenceDiagram.getKey();
                expectedInferenceMeu = influenceDiagram.getValue();

                if (!Objects.equals(expectedInferenceMeu, noExpectedMeu)) {
                    System.out.println("Network in analysis: " + bayesNetworkName);
                    // Open the file containing the network
                    InputStream file = getClass().getClassLoader().getResourceAsStream(bayesNetworkName);
                    // Load the Bayesian network
                    PGMXReader pgmxReader = new PGMXReader();
                    ProbNet probNet = pgmxReader.loadProbNet(file, bayesNetworkName).getProbNet();

                    inferenceAlgorithmLIMID = new LIMIDEvaluation(probNet,algorithmRefinement);
                    //inferenceAlgorithmLIMID.getProbsAndUtilities();
                    inferenceMeu = inferenceAlgorithmLIMID.getGlobalUtility().values[0];
                    System.out.println(inferenceMeu);
                    assertEquals(expectedInferenceMeu, inferenceMeu, maxError);
                }

            } catch (Exception e) {
                printExceptionAndFailIfImplemented(e);
            }
        }
    }

    @SuppressWarnings("unused")
    private TablePotential getTablePotential(Potential potential){
        TablePotential table=null;
        try {
            table = potential.tableProject(null, null).get(0);
        } catch (NonProjectablePotentialException
                | WrongCriterionException e) {
            e.printStackTrace();
        }
        return table;
    }

    /**
     * @param network A probabilistic network
     * @return An InferenceAlgorithm for 'network'. If the network is not evaluable
     * with the algorithm then the test calling this method is skipped.
     */
    @SuppressWarnings("unused")
    private InferenceAlgorithm buildInferenceAlgorithmAndSkipTestIfNotEvaluable(
            ProbNet network) {
        boolean isEvaluable;
        InferenceAlgorithm algorithm = null;

        //If the network is not evaluable then the test is skipped
        isEvaluable = true;
        try {
            algorithm = buildInferenceAlgorithm(network);
        } catch (NotEvaluableNetworkException e1) {
            isEvaluable = false;
        }
        assumeTrue(isEvaluable);
        return algorithm;
    }
    /**
     * @param e An exception
     */
    @SuppressWarnings("restriction")
    protected void printExceptionAndFailIfImplemented(Exception e) {
        if (e.getClass()!=NotImplementedException.class){
            e.printStackTrace();
            fail();
        }
    }

}
